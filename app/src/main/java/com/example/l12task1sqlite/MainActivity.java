package com.example.l12task1sqlite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.ma_sign_in)
    Button signIn;
    @BindView(R.id.ma_create_new)
    Button createNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


    }

    @OnClick (R.id.ma_sign_in)
    public void onClick(){

    }

    @OnClick(R.id.ma_create_new)
    public void onClick(){

    }
}
